<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Audios
 *
 * @ORM\Table(name="audios")
 * @ORM\Entity
 */
class Audios
{
    /**
     * @var string
     *
     * @ORM\Column(name="dialog_id", type="string", length=255, nullable=true)
     */
    public $dialogId;

    /**
     * @var string
     *
     * @ORM\Column(name="lang_code", type="string", length=255, nullable=true)
     */
    public $langCode;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    public $filename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @var string
     * @return string
     */
    public function getDialogId()
    {
        return $this->dialogId;
    }

    /**
     *
     * @param string $dialogId
     */
    public function setDialogId($dialogId)
    {
        $this->dialogId = $dialogId;
    }

    /**
     * @var string
     * @return string
     */
    public function getLangCode()
    {
        return $this->langCode;
    }

    /**
     *
     * @param string $langCode
     */
    public function setLangCode($langCode)
    {
        $this->langCode = $langCode;
    }

    /**
     * @var string
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     *
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     *
     * @var \DateTime
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     *
     * @var \DateTime
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     *
     * @var string
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


}

