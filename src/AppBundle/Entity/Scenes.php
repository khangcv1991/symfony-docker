<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Scenes
 *
 * @ORM\Table(name="scenes")
 * @ORM\Entity
 */
class Scenes
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    public $name;

    /**
     * @var string
     *
     * @ORM\Column(name="version_id", type="string", length=255, nullable=true)
     */
    public $versionId;

    /**
     * @var string
     *
     * @ORM\Column(name="description_id", type="string", length=255, nullable=true)
     */
    public $descriptionId;

    /**
     * @var string
     *
     * @ORM\Column(name="location_id", type="string", length=255, nullable=true)
     */
    public $locationId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    public $sequence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    /**
     * @return string
     */

    public $descriptionTranslation = array();

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getVersionId()
    {
        return $this->versionId;
    }

    /**
     * @param string $versionId
     */
    public function setVersionId($versionId)
    {
        $this->versionId = $versionId;
    }

    /**
     * @return string
     */
    public function getDescriptionId()
    {
        return $this->descriptionId;
    }

    /**
     * @param string $descriptionId
     */
    public function setDescriptionId($descriptionId)
    {
        $this->descriptionId = $descriptionId;
    }

    /**
     * @return string
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * @param string $locationId
     */
    public function setLocationId($locationId)
    {
        $this->locationId = $locationId;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    public function addDescriptionTranslation($texts)
    {

        foreach ($texts as $text) {
            if (isset($this->descriptionTranslation[$text->getLangCode()]) == false) {
                $this->descriptionTranslation[$text->getLangCode()] = $text->getText();
            }
        }
    }

}

