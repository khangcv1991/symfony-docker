<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dialogs
 *
 * @ORM\Table(name="dialogs")
 * @ORM\Entity
 */
class Dialogs
{
    /**
     * @var string
     *
     * @ORM\Column(name="name_id", type="string", length=255, nullable=true)
     */
    public $nameId;

    /**
     * @var string
     *
     * @ORM\Column(name="scene_id", type="string", length=255, nullable=true)
     */
    public $sceneId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    public $sequence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;

    public $nameTranslations = array();

    /**
     * @return string
     */
    public function getNameId()
    {
        return $this->nameId;
    }

    /**
     * @param string $nameId
     */
    public function setNameId($nameId)
    {
        $this->nameId = $nameId;
    }

    /**
     * @return string
     */
    public function getSceneId()
    {
        return $this->sceneId;
    }

    /**
     * @param string $sceneId
     */
    public function setSceneId($sceneId)
    {
        $this->sceneId = $sceneId;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getNameTranslations()
    {
        return $this->nameTranslations;
    }

    /**
     * @param array $nameTranslations
     */
    public function setNameTranslations($nameTranslations)
    {
        $this->nameTranslations = $nameTranslations;
    }

    public function addNameTranslation($texts)
    {
        foreach ($texts as $text) {
            if (isset($this->nameTranslations[$text->getLangCode()]) == false) {
                $this->nameTranslations[$text->getLangCode()] = $text->getText();
            }
        }
    }


}

