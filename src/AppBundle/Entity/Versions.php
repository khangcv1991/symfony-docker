<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Versions
 *
 * @ORM\Table(name="versions")
 * @ORM\Entity
 */
class Versions
{
    /**
     * @var string
     *
     * @ORM\Column(name="version_no", type="string", length=255, nullable=true)
     */
    private $versionNo;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

