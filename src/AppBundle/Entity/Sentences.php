<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sentences
 *
 * @ORM\Table(name="sentences")
 * @ORM\Entity
 */
class Sentences
{
    /**
     * @var string
     *
     * @ORM\Column(name="text_id", type="string", length=255, nullable=true)
     */
    public $textId;

    /**
     * @var string
     *
     * @ORM\Column(name="dialog_id", type="string", length=255, nullable=true)
     */
    public $dialogId;

    /**
     * @var integer
     *
     * @ORM\Column(name="sequence", type="integer", nullable=true)
     */
    public $sequence;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    public $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    public $updatedAt;

    /**
     * @return string
     */
    public function getTextId()
    {
        return $this->textId;
    }

    /**
     * @param string $textId
     */
    public function setTextId($textId)
    {
        $this->textId = $textId;
    }

    /**
     * @return string
     */
    public function getDialogId()
    {
        return $this->dialogId;
    }

    /**
     * @param string $dialogId
     */
    public function setDialogId($dialogId)
    {
        $this->dialogId = $dialogId;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int $sequence
     */
    public function setSequence($sequence)
    {
        $this->sequence = $sequence;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getTextTranslations()
    {
        return $this->textTranslations;
    }

    /**
     * @param array $textTranslations
     */
    public function setTextTranslations($textTranslations)
    {
        $this->textTranslations = $textTranslations;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $id;


    public $textTranslations = array();


    public function addTextTranslation($texts)
    {
        foreach ($texts as $text) {
            if (isset($this->textTranslations[$text->getLangCode()]) == false) {
                $this->textTranslations[$text->getLangCode()] = $text->getText();
            }
        }
    }

}

