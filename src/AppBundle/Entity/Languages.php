<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Languages
 *
 * @ORM\Table(name="languages")
 * @ORM\Entity
 */
class Languages
{
    /**
     * @var string
     *
     * @ORM\Column(name="native_name", type="string", length=255, nullable=true)
     */
    private $nativeName;

    /**
     * @var string
     *
     * @ORM\Column(name="iso_language_name", type="string", length=255, nullable=true)
     */
    private $isoLanguageName;

    /**
     * @var string
     *
     * @ORM\Column(name="is_o6391", type="string", length=255, nullable=true)
     */
    private $isO6391;

    /**
     * @var string
     *
     * @ORM\Column(name="lang_code", type="string", length=6, nullable=true)
     */
    private $langCode;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=true)
     */
    private $version;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=255)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


}

