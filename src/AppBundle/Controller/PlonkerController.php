<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 21/8/17
 * Time: 4:35 AM
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class PlonkerController extends Controller
{

    /**
     * @Route("/api/scripts/v/{no}")
     *
     * @Method("GET")
     */
    function getSceneScript($no)
    {
	exec("rm -r output");
//	exec("mkdir output");
        $logger = $this->get('logger');

        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $fs = new Filesystem();
        $statement = $connection->prepare("USE v".$no."; "."SELECT S.id, S.sequence FROM scenes AS S INNER JOIN versions AS V ON S.version_id = V.id WHERE V.version_no =" . $no . " ORDER BY S.sequence;");
        $statement->execute();
        $scene_results = $statement->fetchAll();
	if(count($scene_results) == 0){
		$response = new Response();
		$response->setContent(json_encode("no data for this version", JSON_UNESCAPED_UNICODE));
        	$response->headers->set('Content-Type', 'application/json');
        	//$response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        	//$response->headers->set('Content-length', filesize($zipName));

        	return $response;
	}
        foreach ($scene_results as $item) {
            $statement = $connection->prepare("USE v".$no."; "."SELECT DISTINCT T.text FROM scenes AS S INNER JOIN versions AS V ON S.version_id = V.id INNER JOIN dialogs AS D ON D.scene_id = S.id INNER JOIN sentences AS SEN ON SEN.dialog_id = D.id INNER JOIN texts as T ON T.text_id = SEN.text_id WHERE V.version_no =" . $no . "  AND T.lang_code =\"en\" AND S.sequence =" . $item['sequence'] . " ORDER BY S.sequence, D.sequence;");
            $statement->execute();
            $sentences_results = $statement->fetchAll();
            $fileName = sprintf("output/audio-s%03d-d000-en.txt", $item['sequence']);
            try {
                $fs->dumpFile($fileName, '');
            } catch (IOExceptionInterface $e) {
                $logger->error("An error occurred while creating your directory at " . $e->getPath());
            }

            foreach ($sentences_results as $sentence) {
                $fs->appendToFile($fileName, $sentence['text']);
                $fs->appendToFile($fileName, "\n");
            }


            $statement = $connection->prepare("USE v".$no."; "."SELECT DISTINCT T.text FROM scenes AS S INNER JOIN versions AS V ON S.version_id = V.id INNER JOIN dialogs AS D ON D.scene_id = S.id INNER JOIN sentences AS SEN ON SEN.dialog_id = D.id INNER JOIN texts as T ON T.text_id = SEN.text_id WHERE V.version_no = 130 AND T.lang_code =\"zh-hz\" AND S.sequence =" . $item['sequence'] . " ORDER BY S.sequence, D.sequence;");
            $statement->execute();
            $sentences_results = $statement->fetchAll();
            $fileName = sprintf("output/audio-s%03d-d000-cn.txt", $item['sequence']);
            try {
                $fs->dumpFile($fileName, '');
            } catch (IOExceptionInterface $e) {
                $logger->error("An error occurred while creating your directory at " . $e->getPath());
            }

            foreach ($sentences_results as $sentence) {
                $fs->appendToFile($fileName, $sentence['text']);
                $fs->appendToFile($fileName, "\n");
            }
        }

        // Get real path for our folder
        $zipName = "script-v" . $no . ".zip";
        $rootPath = realpath('output');

        // Initialize archive object
        $zip = new \ZipArchive();
        $zip->open($zipName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }

        // Zip archive will be created only after closing object
        $zip->close();

        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        $response->headers->set('Content-length', filesize($zipName));

        return $response;
    }


}
